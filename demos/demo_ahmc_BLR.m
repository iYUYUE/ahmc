%% Demo of adaptive HMC for Bayesian Logistic Regression
%
% Ziyu and Shakir, 2013.

%% Data set and seed
% alternative experiments include:
% dataName = 'ripley';
% dataName = 'australian';
% dataName = 'german';
% dataName = 'pima';
dataName = 'heart';
seed = 1;

%% Run AHMC for BLR
[out q] = BLR_hmc(dataName, seed);
samples = out.saved; %samples after burnin

%% Compare MAP and Bayes Estimates
% Compare posterior mean with posterior mode
disp({'Post mode', 'Post mean'});
disp([q mean(samples)']);

%% Trace plots for 3 dimensions
D = 14;
idx = [1 3 5 7];
col = getColorsRGB;
for i =1:length(idx)
    subplot(length(idx),1,i);
    plot(samples(:,idx(i)),'x','Color',col(i,:),'lineWidth',2);
end;

%% All results
% Refer to analysis of results in the paper:
% Z. Wang, S. Mohamed and N. de Freitas, *Adaptive Hamiltonian and Riemann
% Monte Carlo Samplers*. <http://arxiv.org/abs/1302.6182 arXiv Preprint>,
% 2013.
%
% The results for this experiment, see <plotMain.html Plot Result>.
