function [out] = LGC_HMC_LV(seed)

if nargin < 1, seed = ceil(rand*100); end;
setSeed(seed);


%% get data and output directories

%% Setup Model
% Grid Size
N     = 64;

% Load data
% Y is the data
% X are the latent variables
Data = load('./data/TestData64.mat');
y = Data.Y;

% Hyperparameters of model
s     = 1.91;
b     = 1/33;
mu    = log(126) - s/2;
m     = 1/(N^2);

[D] = length(y);

% HMC Setup
NumOfIterations    = 6000;
BurnIn             = 1000;

% Set initial values of w
muOnes   = ones(D,1)*mu;

% Calculate covariance matrix
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
SigmaInv       = zeros(N^2);
Sigma          = zeros(N^2);
x_r            = 0:1/(N - 1):1;
y_r            = 0:1/(N - 1):1;
[xs, ys]       = meshgrid(x_r, y_r);
coord_xy(:, 1) = xs(:);
coord_xy(:, 2) = ys(:);

%%% FASTER %%%
for i=1:N^2,
    coords1    = repmat(coord_xy(i,:), N^2, 1) - coord_xy; 
    Sigma(i,:) = sum(coords1.^2, 2).^0.5;
end

% Free up memory
coords1  = [];
coord_xy = [];

Sigma    = s.*exp( -Sigma./(b*N) );

SigmaInv = chol2inv(chol(Sigma));



%% Run AHMC.
bounds = [0.001, 0.1; 1, 500];
tic
[out] = ahmc(NumOfIterations, BurnIn, bounds, @LGC_grad, @LGC_llh, D);
% Stop timer
TimeTaken = toc;


%% Compute Evaluation Metrics
% 1. Compute ESS

betaPosterior = out.saved;
total_RandomStep = out.total_RandomStep;

numSamples = size(betaPosterior,1);
ESS.allESS = CalculateESS(betaPosterior, numSamples-1);
ESS.minESS = min(ESS.allESS(:));
ESS.medESS = median(ESS.allESS(:));
ESS.maxESS = max(ESS.allESS(:));
fprintf('ESS: Min = %4.2f, Med = %4.2f, Max = %4.2f, Mean = %4.2f\n',...
    ESS.minESS, ESS.medESS, ESS.maxESS, mean(ESS.allESS(:)));

fprintf('ESS/leapfrog: Min = %4.2f, Med = %4.2f, Max = %4.2f, Mean = %4.2f\n', ...
    ESS.minESS/(total_RandomStep/5000), ESS.medESS/(total_RandomStep/5000), ...
    ESS.maxESS/(total_RandomStep/5000), ...
    mean(ESS.allESS(:))/(total_RandomStep/5000));


% 3. Train joint-likelihood
fprintf('Final Train Energy: %2.4f\n',out.energy(end));

% 4. ACF on samples
acf = acorr(betaPosterior); % S-1 x D
fprintf('Lag-3 acorr for 1st covariate: %2.2f\n',acf(3,1));

% 5. Update distance
updateDist = mean(sqrt(sum((betaPosterior(2:end,:) - ...
    betaPosterior(1:end-1,:)).^2,2)));
fprintf('Mean update distance: %2.2f\n',updateDist);



%% Helper Functions   
function [llh] = LGC_llh(x)
    mexpx = m*exp(x);
    llh = -(y'*x - sum(mexpx) - 0.5*((x - muOnes)'*SigmaInv*(x - muOnes)));
end

function [grad] = LGC_grad(x)
    mexpx = m*exp(x);
    grad = -(y - mexpx - SigmaInv*(x - muOnes));
end

end
