function data = getAHMCdata(dataName, dataDir)

switch dataName
    case 'stochVol'
        fname = sprintf('%s/StochVolData1.mat',dataDir);
        in = load(fname);
        data.Y = in.y;
        data.trueBeta = in.beta;
        data.trueX = in.Truex;
        data.truePhi = in.phi;
        data.trueSigma = in.sigma;
        
    case {'german', 'australian', 'pima', 'ripley', 'heart'}
        % Get data
        fname = sprintf('%s%s.mat',dataDir,dataName);
        in = load(fname);
        
        % Shape data
        tmp = in.X(:,1:end-1); % DxN 
        Y = in.X(:,end); % Nx1;
        Xorig = zscore(tmp)'; 
        
        switch dataName
            case 'german'
                nTestPerClass = 50;
                polyOrder = 1;
                Y(Y == 2) = 0; % switch to 1/0 convention
            case 'australian'
                nTestPerClass = 35;
                polyOrder = 1;
            case 'pima'
                nTestPerClass = 25;
                polyOrder = 1;
            case 'ripley'
                nTestPerClass = 15;
                polyOrder = 3;
            case 'heart'
                nTestPerClass = 13;
                polyOrder = 1;
                Y(Y == 2) = 0; % switch to 1/0 convention
        end;
        
        % Create data for poly basis regression if desired
        X = [];
        for i = 1:polyOrder
            X = [X; Xorig.^i];
        end; 
        
        % Create Train and Test data
        ix1 = find(Y == 1);
        testLoc1 = randperm(length(ix1));
        ix0 = find(Y == 0);
        testLoc0 = randperm(length(ix0));
        
        testIx = [ix0(testLoc0(1:nTestPerClass)); ix1(testLoc1(1:nTestPerClass))];
        data.Xtest = X(:,testIx);
        data.Ytest = Y(testIx);
        
        trainIx = [ix0(testLoc0(nTestPerClass+1:end)); ix1(testLoc1(nTestPerClass+1:end))];
        data.X = X(:,trainIx);
        data.Y = Y(trainIx);
    case 'coxSynth'
        data = load(sprintf('%sTestData64.mat',dataDir));
        N = 64;
        beta = 1/33;
        s2 = 1.92;
        
        data.N = N;
        data.m =  1/N^2;
        data.mu = log(126) - s2/2;
        
        % Form covariance matrix
        x_r = 0:1/(N-1):1;
        y_r = 0:1/(N-1):1;
        [xs, ys] = meshgrid(x_r, y_r);
        coord_xy(:, 1) = xs(:);
        coord_xy(:, 2) = ys(:);
        Sig = zeros(N^2,N^2);
        for i=1:N^2,
            coords1 = repmat(coord_xy(i,:), N^2, 1) - coord_xy;
            Sig(i,:) = sum(coords1.^2, 2).^0.5;
        end
        data.Sigma    = s2.*exp( -Sig./(beta*N) );
        data.cholSigma = chol(data.Sigma); % store cholesky for comp
        
    otherwise
        error('Uknown data set - cannot load');
end;