%% Demo of adaptive HMC for a stochastic volaity model
% Shows application of AHMC on high dimensional data. 
%
% Ziyu and Shakir, 2013.

%% Data set and seed
% We use a synthetic data set.
dataName = 'stochVol';
seed = 1;

%% Run AHMC for stochastic volatility
% Note: This takes a long time to run.
out = StochVol_ahmc(dataName, seed);
samples = out.saved; %samples after burnin

%% Analysis
% Refer to analysis of results in the paper:
% Z. Wang, S. Mohamed and N. de Freitas, *Adaptive Hamiltonian and Riemann
% Monte Carlo Samplers*. <http://arxiv.org/abs/1302.6182 arXiv Preprint>, 2013.



