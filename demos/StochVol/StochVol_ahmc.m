function out = StochVol_ahmc(dataName, seed)

close all;
if nargin < 2, seed = log(cputime)*1000; end;
setSeed(seed);

%% Settings
% Get data
data = getAHMCdata(dataName, 'data/');
[R,C] = size(data.Y);
D = R + 3;
data.mode = 'all';

% Get HMC Settings
config = configStochVol_ahmc(dataName);
NumOfIterations =  config.NumOfIterations;
BurnIn = config.BurnIn;
NumOfLeapFrogSteps = config.NumOfLeapFrogSteps;
StepSize = config.StepSize;
saveout = config.saveout;

%% Rum AHMC.
bounds = [1e-4, 1e-2; 1, 300];
gradient = @(p)sv_grad(p, data);
func = @(p)sv_llh(p, data);

tic
[out] = ahmc(NumOfIterations, BurnIn, bounds, gradient, func, D);
out.rtime = toc;


%% Compute Evaluation Metrics.
total_RandomStep = out.total_RandomStep;
modelPosterior = out.saved;
fprintf('Time taken: %2.2f mins\n', out.rtime/60);

%% Compute Evaluation Metrics
% 1. Compute ESS
numSamples = size(modelPosterior,1);
ESS.allESS = CalculateESS(modelPosterior, numSamples-1);

ESS.minESS = min(ESS.allESS(:));
ESS.medESS = median(ESS.allESS(:));
ESS.maxESS = max(ESS.allESS(:));

num_samples = NumOfIterations-BurnIn;
fprintf('ESS/leapfrog: Min = %4.2f, Med = %4.2f, Max = %4.2f, Mean = %4.2f\n', ...
    ESS.minESS/(total_RandomStep/num_samples), ...
    ESS.medESS/(total_RandomStep/num_samples), ...
    ESS.maxESS/(total_RandomStep/num_samples), ...
    mean(ESS.allESS(:))/(total_RandomStep/num_samples));

ESS.minESSX = min(ESS.allESS(1:end-3));
ESS.medESSX = median(ESS.allESS(1:end-3));
ESS.maxESSX = max(ESS.allESS(1:end-3));
fprintf('ESS: Min = %4.2f, Med = %4.2f, Max = %4.2f, Mean = %4.2f\n',...
    ESS.minESSX, ESS.medESSX, ESS.maxESSX, mean(ESS.allESS(1:end-3)));

fprintf('ESS: Phi = %4.2f, Beta = %4.2f, Sigma = %4.2f\n',...
    ESS.allESS(end-2), ESS.allESS(end-1), ESS.allESS(end));

% 2. Compute Test Error
T = length(data.trueX);
w = modelPosterior(end, :);
v1 = [data.trueX' data.truePhi, data.trueBeta, data.trueSigma];
v2 = [w(1:T), tanh(w(end-2)), w(end-1) exp(w(end))];
err.MAE = mean(abs(v1-v2));
fprintf('Error: mae = %2.4f\n',err.MAE);

% 3. Train joint-likelihood
fprintf('Final Train Energy: %2.4f\n',out.energy(end));

% 4. ACF on samples
acf = acorr(modelPosterior); % S-1 x D
fprintf('Lag-3 acorr for 1st covariate: %2.2f\n',acf(3,1));

% 5. Update distance
updateDist = mean(sqrt(sum((modelPosterior(2:end,:) -...
    modelPosterior(1:end-1,:)).^2,2)));
fprintf('Mean update distance: %2.2f\n',updateDist);


function [grad] = sv_grad(p, data)
    [~, grad] = stochasticVol(p, data); grad = -grad';
end

function [llh] = sv_llh(p, data)
    llh = stochasticVol(p, data); llh = -llh;
end

end