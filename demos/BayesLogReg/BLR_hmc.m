%% Run AHMC for Bayesian logistic regression
% 5 data sets can be used: german, australian, ripley, pima, heart
% We include the configuration setting we used, and these can be changed in
% the file confugBLR_hmc.m.
%

function [out q] = BLR_hmc(dataName, seed)

if nargin < 2, seed = log(cputime)*1000; end;
setSeed(seed);

%% Experiment settinngs.

% Get HMC Settings
config = configBLR_hmc(dataName);
NumOfIterations =  config.NumOfIterations;
BurnIn = config.BurnIn;

% Get data
data = getAHMCdata(dataName, 'data/');
[D,N] = size(data.X);
t = data.Y;
XX = [ones(N,1), data.X'];
D= D+1;
alpha = 100;

%% Run AHMC
gradient = @(p)blr_grad(p, XX, alpha);
func = @(p)blr_loglik(p, XX, t, alpha);
bounds = [0.01, 0.2; 1, 100];

tic
[out] = ahmc(NumOfIterations, BurnIn, bounds, gradient, func, D);
out.rtime = toc;
fprintf('\n');
fprintf('\n Evaluation metrics: \n');

%% Compute Evaluation Metrics
% we use ESS per leapfrog step in the paper.
total_RandomStep = out.total_RandomStep;
fprintf('Total number of leapfrogs Performed: %d\n', total_RandomStep);
fprintf('Time taken: %2.2f mins\n', out.rtime/60);

%% Compute ESS
numSamples = size(out.saved,1);
ESS.allESS = CalculateESS(out.saved, numSamples-1);
ESS.minESS = min(ESS.allESS(:));
ESS.medESS = median(ESS.allESS(:));
ESS.maxESS = max(ESS.allESS(:));

fprintf('ESS: Min = %4.2f, Med = %4.2f, Max = %4.2f, Mean = %4.2f\n', ...
    ESS.minESS, ESS.medESS, ESS.maxESS, mean(ESS.allESS(:)));

%% ESS per Leapfrog
fprintf('ESS/leapfrog: Min = %4.2f, Med = %4.2f, Max = %4.2f, Mean = %4.2f\n', ...
    ESS.minESS/(total_RandomStep/5000), ESS.medESS/(total_RandomStep/5000), ...
    ESS.maxESS/(total_RandomStep/5000), ...
    mean(ESS.allESS(:))/(total_RandomStep/5000));

%% Compute Test Error
[ystar err] = bayesLogRegPred(data.Xtest,out.saved,data.Ytest);
fprintf('Error: zoe = %2.4f, loglik = %2.4f\n',err.zoe,err.testLogLik);

%% Train joint-likelihood
fprintf('Final Train Energy: %2.4f\n',out.energy(end));

%% ACF on samples
acf = acorr(out.saved); % S-1 x D
fprintf('Lag-3 acorr for 1st covariate: %2.2f\n',acf(3,1));

%% Update distance
updateDist = mean(sqrt(sum((out.saved(2:end,:) - out.saved(1:end-1,:)).^2,2)));
fprintf('Mean update distance: %2.2f\n',updateDist);


%% Max Lik
options.display = 'none';
options.maxFunEvals = ceil(bounds(2,2)*2);
[q, f] = minFunc(@objective, rand(D, 1), options);

%% Helper functions
% Gradient of the negative log likelihood.
function [grad] = blr_grad(p, XX, alpha)
    D = size(XX, 2);
    f = XX*p;

    grad = -( XX'*( t - (1./(1+exp(-f))) ) - eye(D)*(1/alpha)*p );


end

% Negative log likelihood.
function [llh] = blr_loglik(p, XX, t, alpha)
    D = size(XX, 2);
    LogPrior      = LogNormPDF(zeros(1,D),p,alpha);
    f             = XX*p;
    LogLikelihood = f'*t - sum(log(1+exp(f))); %training likelihood
    llh    = -(LogLikelihood + LogPrior);
end

function [f, g] = objective(x)
    f = func(x); g = gradient(x);
end

end