%% Demonstrate sensitivity of HMC to parameter settings

% Specify Gaussian model
target_sigma = [1, 0.99; 0.99, 1];
target_mean = [3; 5];
func = inline('transpose(x-target_mean)*inv(target_sigma)*(x-target_mean)/2');
func = @(x)func(target_mean, target_sigma, x);
gradient  = inline('inv(target_sigma)*(x-target_mean)');
gradient = @(x)gradient(target_mean, target_sigma, x);

% Convergence Behavior
% The good: l = 40 epsilon = 0.05
% The bad:  l = 50 epsilon = 0.05

% Behavior after convergence
% The good: l = 40 epsilon = 0.16
% The bad:  l = 50 epsilon = 0.16
% The ugly: l = 50 epsilon = 0.15

% epsilon = 0.16; l = 40; % good
epsilon = 0.16; l = 50; % bad
epsilon = 0.2; l = 8; % ugly


max_iter = 5000;
q = [-5; -5];
qs = zeros(max_iter, 2);
accept_window = zeros(max_iter, 1);
qs(1, :) = q';
total_accept = 0;

sigmaCholR = chol(target_sigma, 'lower');

for i = 1:max_iter
    [q, accept, q_prop] = hmc_iter(l, epsilon, q, gradient, func);
    % [q, accept, q_prop] = hmc_iter(l, epsilon, q, gradient, func, ...
    %     sigmaCholR);
    qs(i+1, :) = q';
    total_accept = total_accept + accept;
end

C = acorrtime(qs);
C = acorr(qs, max_iter - 1);
    

    
figure(1); clf;

mean(qs)
cov(qs)

xx = qs(:,1);
yy = qs(:,2);
scatter(xx, yy, 4);
p = plot(xx(:), yy(:),'x');
set(p,'Color','red','LineWidth',1)
set(gca,'FontSize',12);
title(sprintf('\\epsilon = %1.2f, L = %d',epsilon,l),'fontsize', 14, 'FontWeight','bold', 'Interpreter', 'Tex');
hold on;
x1 = 0:.01:6; x2 = 2:.01:8;
[X,Y] = meshgrid(x1,x2);
F = mvnpdf([X(:) Y(:)], target_mean', target_sigma);
F = reshape(F,length(x2),length(x1));
contour(x1, x2, F, 5);
grid on;

% 2. Plot auto-correlation
figure(2); clf;
plot(C(1:100, 1), 'LineWidth',2);
grid on;
set(gca,'Ylim',[-0.2, 1])
xlim([0 100]);
set(gca,'FontSize',12);
xlabel('Lag', 'fontsize', 14, 'FontWeight','bold');


