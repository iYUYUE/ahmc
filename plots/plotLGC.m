%% Plot results for log Gaussian cox process (part I)
% Compare Adaptive HMC vs NUTS, and Adaptive RMHMC vs RMHMC for the
% log-Gaussian Cox Point Process
%
% The figure produced here correspond to figure 3 in the paper
% 
% Shakir and Ziyu, February 2013

%%

makePDF = 0; % set to print and put PDF in pdf/ directory

% Change these directories if needed
outDir = 'plots';
% Not needed if processed results stored
dirList = {'/media/bigguy/projects/HMC/Out/ARMHMC', ...
    '/media/bigguy/projects/HMC/Out/RMHMC', ...
    '/media/bigguy/projects/HMC/Out/NUTS', ...
    '/media/bigguy/projects/HMC/Out/AHMC'};

% Algorithms and seeds used
mcmcList = {'LGC_ARMHMC', 'LGC_RMHMC', 'logGaussCox_coxSynth', 'LGC_AHMC'};
seedList = [1 1000 2000 2012 3000 4000 5000 500 50 6000];
numSamples = 5000;

%% Load plotting data
%
% Plotting file: *plotLGC.m*

if exist('plotData_LGC.mat','file')
    % Load pre-saved data
    fprintf('Loading ploting data for LGC model ...\n');
    load('plotData_LGC');
else
    % Extract raw Data
    for j = 1:length(dirList)
        dir = dirList{j};
        mcmc = mcmcList{j};
        for i = 1:length(seedList)
            seed = seedList(i);
            fname = sprintf('%s/%s_%d',dir, mcmc, seed)
            dat = load(fname);
            minESS(i,j) = dat.ESS.minESS;
            maxESS(i,j) = dat.ESS.maxESS;
            medESS(i,j) = dat.ESS.medESS;
            Lsteps(i,j) = dat.total_RandomStep;
        end;
    end;
    save('LGC_plotData', 'minESS', 'maxESS', 'medESS', 'Lsteps');
end;


%% Plot graphs
% Plots and prints graphs as separate pdf images.
% Points below the diagonal line indicate that the adaptive method has
% better (higher) ESS/L than the non-adapive methood to which it is
% compared.
%
% Comparing minimum (red), median (blue) and maximum (black) ESS/L for 
% the Log-Gaussian Cox model. Each of the colored glyphs represents one of
% the 10 chains generated.

shape = 'sdoh*';
col = 'rbkmg';

%% AHMC vs NUTS
figure;
mm = 30;
scaleNUTS = Lsteps(:,3)./numSamples;
scaleAHMC = Lsteps(:,4)./numSamples;

plot(minESS(:,4)./scaleAHMC, minESS(:,3)./scaleNUTS, col(1),'LineStyle','none','Marker',shape(1), 'LineWidth',2, 'MarkerSize', 10);
hold on;
plot(medESS(:,4)./scaleAHMC,medESS(:,3)./scaleNUTS, col(2),'LineStyle','none','Marker',shape(2), 'LineWidth',2, 'MarkerSize', 10);
hold on;
plot(maxESS(:,4)./scaleAHMC,maxESS(:,3)./scaleNUTS, col(3),'LineStyle','none','Marker',shape(3), 'LineWidth',2, 'MarkerSize', 10);
hold on;
plot(0:1e-3:mm,0:1e-3:mm,'--','LineWidth',2, 'Color',[0.8 0.8 0.8]);

ylabel('NUTS','FontSize',14, 'FontWeight','bold');
xlabel('AHMC','FontSize',14, 'FontWeight','bold');
title('ESS/L','FontSize',14, 'FontWeight','bold');
set(gca,'FontSize',12);
legend({'Minimum', 'Median', 'Maximum'},'Location', 'NorthWest');
grid on; axis square;
xlim([0 mm]);
ylim([0 mm]);

if makePDF
    outName = 'essL_AHMCvsNUTS_LGC';
    printPDF(outName,outDir);
end;

%% ARMHMC vs RMHMC
% No legend here, meant to be seen next to previous plot
figure;
mm = 700;
scaleARM = Lsteps(:,1)./numSamples; % set to 1 to see raw ESS
scaleRMHMC = Lsteps(:,2)./numSamples;
plot(minESS(:,1)./scaleARM, minESS(:,2)./scaleRMHMC,col(1),'LineStyle','none','Marker',shape(1), 'LineWidth',2, 'MarkerSize', 10);
hold on;
plot(medESS(:,1)./scaleARM, medESS(:,2)./scaleRMHMC,col(2),'LineStyle','none','Marker',shape(2), 'LineWidth',2, 'MarkerSize', 10);
hold on;
plot(maxESS(:,1)./scaleARM, maxESS(:,2)./scaleRMHMC,col(3),'LineStyle','none','Marker',shape(3), 'LineWidth',2, 'MarkerSize', 10);
hold on
plot(0:1e-3:mm,0:1e-3:mm,'--','LineWidth',2, 'Color',[0.8 0.8 0.8]);
ylabel('RMHMC','FontSize',14, 'FontWeight','bold');
xlabel('ARMHMC','FontSize',14, 'FontWeight','bold');
title('ESS/L','FontSize',14, 'FontWeight','bold');
set(gca,'FontSize',12);
grid on; axis square;
xlim([0 mm]);
ylim([0 mm]);

if makePDF
    outName = 'essL_ARMHMCvsRMHMC_LGC';
    printPDF(outName,outDir);
end;






