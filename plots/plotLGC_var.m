%% Plot results for log Gaussian cox process (part II)
% Compare inferred latent field and ariance of posterior samples for
% Adaptive HMC vs NUTS log-Gaussian Cox Point Process.
%
% The figure produced here correspond to figure 4 in the paper.
%
% Shakir and Ziyu, February 2013

%%

makePDF = 0; % set to print and put PDF in pdf/ directory
outDir = 'plots';

%% Extract Raw Data
savName = 'plotData_LGC_heatmaps.mat';
if exist(savName,'file')
    % plotting file: *plotLGC_var.m*
    fprintf('Loading data ...\n');
    load(savName);
else
    % AHMC
    AHMC_ex = load('../AHMC/LGC_AHMC_3000.mat');
    % NUTS
    NUTS_ex = load('../NUTS/logGaussCox_coxSynth_3000.mat');
    
    mean1 = mean(AHMC_ex.betaPosterior(:, :), 1);
    var1 = var(AHMC_ex.betaPosterior(:, :), 1);
    mean2 = mean(NUTS_ex.samples(:, :), 1);
    var2 = var(NUTS_ex.samples(:, :), 1);
    save(savName,'mean1','var1','mean2','var2');
end;

%% Compare latent fields and variances.
% The top row shows the true latent fields. From the true
% data observations (shown in top right corner), we see
% that there are few data points in this region and thus
% we expect to have a high variance in this region. The
% average of the samples obtained using AHMC shows
% that we can accurately obtain samples from the latent
% field x, and that the samples have a variance matching
% our expectations. While NUTS is able to also produce
% good samples of the latent field, the variance of the
% field is not well captured (bottom right image).

% This code thanks to Girolami and Calderhead.
PlotTrueAndEstimated(mean1, var1, mean2, var2);

if makePDF
    outName = 'heatmaps_LGC';
    printPDF(outName,outDir);
end;
