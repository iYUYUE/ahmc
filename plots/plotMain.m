%% Reproduce plots for Adaptive Hybrid Monte Carlo
% Code to reproduce plots in paper:
% Z. Wang, S. Mohamed and N. de Freitas, *Adaptive Hamiltonian and Riemann
% Monte Carlo Samplers*. <http://arxiv.org/abs/1302.6182 arXiv Preprint>, 2013.
%
% Shakir and Ziyu, 2013

%% Download Code
%
% * <../AHMCplots.zip *AHMCplots.zip*>


% Click on links to see results for each.

%% HMC Parameter Sensitivity (fig 1)
% <HMCsensitivity.html plotHMCsensitivity;>

%% Bayesian Logistic Regression using five data sets (fig 2)
% <plotBLR.html plotBLR;>

%% Log-Gaussian Cox Process: ESS comparison (fig 3)
% <plotLGC.html plotLGC;>

%% Log-Gaussian Cox Process: Inferred latent field and variance (fig 4)
% <plotLGC_var.html plotLGC_var>
