function[] = PlotTrueAndEstimated(xEstimates, xVar, xEstimates2, xVar2)

% xEstimates is the mean of the samples of x - i.e. N^2 by 1 vector
% xVar is the variance of the the samples of x - i.e. N^2 by 1 vector

N = 64;

% Y is the data
% X is the latent field
k = 2;
if nargin > 2
    k = 3;
end


% Load data
Data=load('TestData64.mat');
x = Data.X;
y = Data.Y;


figure(5)
Results = x;
hs = tight_subplot(3,3);
% subplot(k,3,1)
axes(hs(1));
imagesc(reshape(Results,N,N));
set(gca,'Xtick',[],'XtickLabel',[],'Ytick',[],'YtickLabel',[]);
ylabel('True Data','FontSize',14, 'FontWeight','bold');
title('Latent Field x','FontSize',14, 'FontWeight','bold','Interpreter','Tex');

% subplot(k,3,2)
axes(hs(2));
imagesc(reshape(exp(Results)./N^2,N,N));
set(gca,'Xtick',[],'XtickLabel',[],'Ytick',[],'YtickLabel',[]);
title('Latent Process \lambda','FontSize',14, 'FontWeight','bold','Interpreter','Tex');


% subplot(k,3,3)
axes(hs(3));
imagesc(reshape(y,N,N));
set(gca,'Xtick',[],'XtickLabel',[],'Ytick',[],'YtickLabel',[]);
title('Variance','FontSize',14, 'FontWeight','bold','Interpreter','Tex');


Results = xEstimates;
% subplot(k,3,4)
axes(hs(4));
imagesc(reshape(Results,N,N));
set(gca,'Xtick',[],'XtickLabel',[],'Ytick',[],'YtickLabel',[]);
ylabel('AHMC','FontSize',14, 'FontWeight','bold');


% subplot(k,3,5)
axes(hs(5));
imagesc(reshape(exp(Results)./N^2,N,N));
set(gca,'Xtick',[],'XtickLabel',[],'Ytick',[],'YtickLabel',[]);


% subplot(k,3,6)
axes(hs(6));
imagesc(reshape(xVar,N,N));
set(gca,'Xtick',[],'XtickLabel',[],'Ytick',[],'YtickLabel',[]);


if nargin > 2
    Results = xEstimates2;
%     subplot(3,3,7)
axes(hs(7));
    imagesc(reshape(Results,N,N));
    set(gca,'Xtick',[],'XtickLabel',[],'Ytick',[],'YtickLabel',[]);
    ylabel('NUTS','FontSize',14, 'FontWeight','bold');
    
%     subplot(3,3,8)
axes(hs(8));
    imagesc(reshape(exp(Results)./N^2,N,N));
    set(gca,'Xtick',[],'XtickLabel',[],'Ytick',[],'YtickLabel',[]);
    
%     subplot(3,3,9)
    axes(hs(9));
    imagesc(reshape(xVar2,N,N));
    set(gca,'Xtick',[],'XtickLabel',[],'Ytick',[],'YtickLabel',[]);
    
end



end